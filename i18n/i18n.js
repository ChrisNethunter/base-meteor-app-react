import i18n from 'i18next'
import Backend from 'i18next-http-backend'
import LanguageDetector from 'i18next-browser-languagedetector'
import { initReactI18next } from 'react-i18next'
import enTranslation from './en.json';
import esTranslation from './es.json';

const resources = {
	en: {
		translation: enTranslation,
	},
	es: {
		translation: esTranslation,
	},
};

i18n.use(Backend).use(LanguageDetector).use(initReactI18next).init({
	resources,
	lng: localStorage.getItem("i18nextLng") || "es",
	debug: false,
	detection: {
		order: ['queryString', 'cookie'],
		cache: ['cookie']
	},
	keySeparator: true,
	interpolation: {
		escapeValue: false
	}
});

export default i18n

