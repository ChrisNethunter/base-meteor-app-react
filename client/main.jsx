import '/imports/startup/client';
import '/imports/startup/both';
import "../i18n/i18n";

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle';
import '/imports/ui/styles/styles.css';
import '/imports/ui/styles/admin_net.css';
import '/imports/ui/styles/hover.css';