import React from 'react';
import { Meteor } from 'meteor/meteor';

import { LocalStorage } from './helpers/LocalStorage';
import { Request } from './helpers/Request';
import { Validations } from './helpers/Validations';
import { AlertNotification } from './helpers/Alerts';
import { Container } from './components/Index';


import FormDinamyc from './components/Forms/FormDinamyc';

let options_select_test = [
  {
    name: 'name 1',
  },
  {
    name: 'name 4',
  },
  {
    name: 'name 3',
  }
]

import './styles/alerts.css';
//smart component
class App extends (React.Component) {

  constructor(props) {
    super(props);
    this.state = {
      formCreateTest: {
        title: 'Form Test',
        rows: true,
        propsHtml: {
          'id': 'form-test',
          'onSubmit': this.handlerSubmitFormTest,
          'className': 'my-class-form'
        },
        buttons: {
          buttonSubmit: {
            text: 'ENVIAR',
            propsHtml: {
              className: 'btn btn-primary btn-lg btn-block',
            },
            block:true
          },
          buttonCancel: {
            text: 'CANCELAR',
            propsHtml: {
              onClick: this.handlerClickCancel,
              className: 'btn btn-secondary btn-lg btn-block'
            },
            block:true
          }
        },
        inputs: [
          {
            label: 'Test',
            cols: {
              md: 6,
              sm: 3,
            },
            propsHtml: {
              'id': 'test',
              'data-message-validation': 'El campo nombre es requerido',
              'required': false,
              'required-regex': 'true',
              'type': 'text',
              'onKeyUp': this.handlerKeyUp,
            }
          },
          {
            label: 'Test',
            cols: {
              md: 6
            },
            propsHtml: {
              'id': 'test7',
              'data-message-validation': 'message invalid type',
              'required': false,
              'required-regex': 'true',
              'type': 'text',
              'onKeyUp': this.handlerKeyUp,
            }
          },

          {
            label: 'Test',
            cols: {
              md: 3
            },
            propsHtml: {
              'id': 'test8',
              'data-message-validation': 'message invalid type',
              'required': false,
              'required-regex': 'true',
              'type': 'text',
              'onKeyUp': this.handlerKeyUp,
            }
          },
          {
            label: 'Test 2',
            cols: {
              md: 3
            },
            propsHtml: {
              'id': 'test2',
              'data-message-validation': 'message invalid type',
              'required': false,
              'required-regex': 'true',
              'type': 'number',
              'onKeyUp': this.handlerKeyUp,
            }
          },
          {
            label: 'Select Test 3',
            cols: {
              md: 3
            },
            propsHtml: {
              'id': 'test3',
              'data-message-validation': 'message invalid type',
              'required': false,
              'required-regex': 'true',
              'type': 'select',
              'onKeyUp': this.handlerChangeSelect,
            },
            options: options_select_test
          }, 
          {
            label: 'Color test',
            cols: {
              md: 3
            },
            propsHtml: {
              'id': 'test4',
              'data-message-validation': 'message invalid type',
              'required': false,
              'required-regex': 'false',
              'type': 'color',
              'onKeyUp': this.handlerKeyUp,
            }
          },
          {
            label: 'Checked test',
            propsHtml: {
              'id': 'test5',
              'data-message-validation': 'message invalid type',
              'required': false,
              'required-regex': 'true',
              'type': 'checkbox',
              'onKeyUp': this.handlerKeyUp,
            }
          },
        ]
      },

    }
  }

  handlerSubmitFormTest = (event) => {
    event.preventDefault();
    //FOREVER
    let target = event.currentTarget;
    let validateForm = Validations.validateForm(target.elements);
    if (!validateForm.status) return false

    console.log(Validations.getDataForm(target.elements));
    //FOREVER
    console.log('perfect')
  }

  handlerKeyUp = (event) => {
    event.preventDefault();
    console.log('key up')
  }

  handlerClickCancel = (event) => {
    event.preventDefault();
    console.log('cancel')
  }

  handlerChangeSelect = (event) => {
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <Container>
          <br />
          <h1> Web home </h1>
          <FormDinamyc formProps={this.state.formCreateTest} />
        </Container>
      </div>
    );
  }
}

export default App
