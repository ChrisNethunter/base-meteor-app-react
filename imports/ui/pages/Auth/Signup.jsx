
import React, { useState, useEffect } from 'react';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import { AlertNotification } from '../../helpers/Alerts';
import { Button, Input } from '../../components/Index';

export default Login = () => {
  const [email, setEmail] = useState(""),
    [password, setPassword] = useState("");
  [confirmPassword, setConfirmPassword] = useState("");

  useEffect(() => {
    validateSession();
  });

  const handlerSubmitLogin = e => {
    e.preventDefault();
    let emailL = email.trim(),
      passwordL = password.trim();

    if (emailL == '' || passwordL == '') {
      AlertNotification.notifyAlert({
        type: 'danger',
        icon: 'fa fa-window-close',
        message: 'The fields must not be empty'
      });
      return false
    }

    Meteor.loginWithPassword(emailL, passwordL, (err, res) => {
      if (err) {
        if (err.message === 'User not found [403]') {
          AlertNotification.notifyAlert({
            type: 'warning',
            icon: 'fa fa-window-close',
            message: 'User not found'
          });
        } else {
          AlertNotification.notifyAlert({
            type: 'warning',
            icon: 'fa fa-window-close',
            message: 'Incorrect user or password'
          });
        }
      } else {
        FlowRouter.go('App')
      }
    });
  }

  const validateSession = () => {
    if (Meteor.userId()) {
      FlowRouter.go('App');
    }
  }

  return (
    <section className="gradient-custom">
      <div className="container py-5 h-100">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-12 col-md-8 col-lg-6 col-xl-5">
            <div className="card bg-dark text-white" style={{ borderRadius: '1rem' }}>
              <div className="card-body p-5">
                <form onSubmit={handlerSubmitLogin} className="mb-md-5 mt-md-4 pb-5">
                  <h2 className="mb-2 text-center">Sign Up</h2>
                  <p className="text-white-50 mb-5 text-center">Meteor { Meteor.settings.public.versionMeteor } + React</p>
                  <div className="form-group">
                    <Input
                      label={'Email'}
                      propsHtml={
                        {
                          type:"email",
                          name:"email",
                          id:"email",
                          placeholder:"email@example.com",
                          onChange:(e) => setEmail(e.target.value)
                        }
                      }
                      classCustom="form-control form-control-lg input-mt"/>
                  </div>
                  <br />
                  <div className="form-group mb-4">
                    <Input
                      label={'Password'}
                      propsHtml={
                        {
                          type:"password",
                          name:"password",
                          id:"password",
                          placeholder:"enter your passsword",
                          onChange:(e) => setPassword(e.target.value)
                        }
                      }
                      classCustom="form-control form-control-lg input-mt"/>
                  </div>
                 
                  <div className="form-group mb-4">
                    <Input
                      label={'Confirm password'}
                      propsHtml={
                        {
                          type:"confirm_password",
                          name:"confirm_password",
                          id:"confirm_password",
                          placeholder:"confirm passsword",
                          onChange:(e) => setConfirmPassword(e.target.value)
                        }
                      }
                      classCustom="form-control form-control-lg input-mt"/>
                  </div>

                  <div className='text-center'>
                    <Button
                      text={'Create account'}
                      classCustom={"btn-outline-light btn-lg px-5"}
                      propsHtml={
                        {
                          name:"login",
                          id:"login",
                          type:"submit",
                          defaultValue:"Login",
                      }}/>
                  </div>

                  <div className="d-flex justify-content-center text-center mt-4 pt-1">
                    <a href="#!" className="text-white"><i className="fab fa-facebook-f fa-lg" /></a>
                    <a href="#!" className="text-white"><i className="fab fa-twitter fa-lg mx-4 px-2" /></a>
                    <a href="#!" className="text-white"><i className="fab fa-google fa-lg" /></a>
                  </div>
                  
                </form>
                <div>
                  <p className="mb-0 text-center">
                    You already have an account ? <a href="/login" className="text-white-50 fw-bold">Login here</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )

}

