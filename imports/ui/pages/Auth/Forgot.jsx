import React, { useState, useEffect } from 'react';
import { AlertNotification } from '../../helpers/Alerts';
import { Button, Input } from '../../components/Index';

export default Forgot = () => {
  const [email, setEmail] = useState("");

  const handlerSubmitForgot = e => {
    e.preventDefault();

    email = email.trim();
    if (email == '') {
      AlertNotification.notifyAlert({
        type: 'danger',
        icon: 'fa fa-window-close',
        message: 'The fields must not be empty'
      });
      return false
    }

    Accounts.forgotPassword({ email: email }, (err) => {
      if (err) {
        if (err.message === 'User not found [403]') {
          AlertNotification.notifyAlert({
            type: 'danger',
            icon: 'fa fa-window-close',
            message: 'This email does not exist'
          });
        } else {
          AlertNotification.notifyAlert({
            type: 'danger',
            icon: 'fa fa-window-close',
            message: err.message
          });
        }
      } else {
        AlertNotification.notifyAlert({
          type: 'success',
          icon: 'fa fa-check',
          message: 'Well done! Email sent, check your email!'
        });
      }
    });
  }

  return (
    <section className="gradient-custom">
      <div className="container py-5 h-100">
        <div className="row d-flex justify-content-center align-items-center h-100">
          <div className="col-12 col-md-8 col-lg-6 col-xl-5">
            <div className="card bg-dark text-white" style={{ borderRadius: '1rem' }}>
              <div className="card-body p-5">
                <form onSubmit={handlerSubmitForgot} className="mb-md-5 mt-md-4 pb-5">
                  <h2 className="mb-2 text-center">Sign In</h2>
                  <p className="text-white-50 mb-5 text-center">Meteor { Meteor.settings.public.versionMeteor }  + React</p>
                  <div className="form-group">
                    <Input
                      label={'Email'}
                      propsHtml={
                        {
                          type:"email",
                          name:"email",
                          id:"email",
                          placeholder:"email@example.com",
                          onChange:(e) => setEmail(e.target.value)
                        }
                      }
                      classCustom="form-control form-control-lg input-mt"/>
                  </div>
                  
                  <br />
                  <div className='text-center'>
                    <Button
                      text={'Send email'}
                      classCustom={"btn-outline-light btn-lg px-5"}
                      propsHtml={
                        {
                          name:"login",
                          id:"login",
                          type:"submit",
                          defaultValue:"Login",
                      }}/>
                  </div>

                </form>
                <div>
                  <p className="mb-0 text-center">
                    You already have an account ? <a href="/login" className="text-white-50 fw-bold">Login here</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

