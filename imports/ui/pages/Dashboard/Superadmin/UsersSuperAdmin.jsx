import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import Swal from 'sweetalert2';

import { Button, Input , Container , Toolbar} from '../../../components/Index';
import FormDinamyc from '../../../components/Forms/FormDinamyc';
import { Validations } from '../../../helpers/Validations';
import { AlertNotification } from '../../../helpers/Alerts';

class UsersSuperAdmin extends Component {
  constructor(props) {
    super(props)
    this.state = {
      show_create : false,
      formCreateUser: {
        title: 'New user',
        rows: true,
        propsHtml: {
          'id': 'form-user',
          'onSubmit': this.handlerSubmit,
          'className': 'my-class-form'
        },
        buttons: {
          buttonSubmit: {
            text: 'Create',
            propsHtml: {
              className: 'btn btn-primary btn-lg btn-block',
            },
            block:true
          },
          buttonCancel: {
            text: 'Cancel',
            propsHtml: {
              onClick: this.handlerClickToggle,
              className: 'btn btn-secondary btn-lg btn-block'
            },
            block:true
          }
        },
        inputs: [
          {
            label: 'Name',
            cols: {
              md: 12,
              sm: 12,
            },
            propsHtml: {
              'id': 'test',
              'data-message-validation': 'Required name',
              'required': false,
              'required-regex': 'true',
              'type': 'text'
            }
          },
          {
            label: 'Password',
            cols: {
              md: 12,
              sm: 12,
            },
            propsHtml: {
              'id': 'password',
              'data-message-validation': 'Required name',
              'required': false,
              'required-regex': 'true',
              'type': 'password'
            }
          },
          {
            label: 'Email',
            cols: {
              md: 12,
              sm: 12,
            },
            propsHtml: {
              'id': 'email',
              'data-message-validation': 'Required email',
              'required': false,
              'required-regex': 'true',
              'type': 'email',
              'placeholder': "email@email.com",
            }
          },
          {
            label: 'Rol',
            cols: {
              md: 12
            },
            propsHtml: {
              'id': 'rol',
              'data-message-validation': 'Required rol',
              'required': false,
              'required-regex': 'true',
              'type': 'select',
              'onKeyUp': this.handlerChangeSelect,
            },
            options: [
              {
                id : 'superadmin',
                name : 'superadmin'
              },
              {
                id : 'client',
                name : 'client'
              }
            ]
          },
        ]
      },
      users:[]
    }
  }

  componentDidMount() {
    this.getUsersAdmin();
  }

  handlerClickToggle = () => {
    this.setState({ show_create : !this.state.show_create })
  }

  handlerSubmit = (event)=>{
    event.preventDefault();
    let target = event.currentTarget;
    let validateForm = Validations.validateForm(target.elements);
    if (!validateForm.status) return false

    let user = Validations.getDataForm(target.elements);

    Meteor.call('create.user.account',user,(error,response)=>{
      if(!error){
        AlertNotification.notifyAlert({
          type: 'success',
          icon: 'fa fa-check',
          message: 'Well done, the user has been created correctly'
        });
        this.getUsersAdmin();
        this.handlerClickToggle();
      }else{
        console.log({error})
        AlertNotification.notifyAlert({
          type: 'danger',
          icon: 'fa fa-window-close',
          message: error.error
        });
      }
    })
  }

  handlerDelete=(event)=>{
    event.preventDefault();
    let id=event.currentTarget.id;
    Swal.fire({
      title: 'Are you sure to delete this user ?',
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        Meteor.call('delete.user', id, (err, response) => {
          if (!err) {
            AlertNotification.notifyAlert({
              type: 'success',
              icon: 'fas fa-check',
              message: 'User delete.'
            });
            this.getUsersAdmin();
          } else {
            AlertNotification.notifyAlert({
              type: 'danger',
              icon: 'fa fa-window-close',
              message: err.error
            });
          }
        })
      }
    })
  }

  getUsersAdmin = () => {
    Meteor.call('get.users.roles',['superadmin'],(error,response) =>{
      if(!error){
        this.setState({users:response});
      }
    })
  }

  render() {
    return (
      <Container >
        {
          this.state.show_create ?
          ""
          :
          <div>
            <Toolbar name={"Users platform"}/> 
            <br />
            <Button
              text={'Create user platform'}
              classCustom={"btn-primary btn-lg px-5"}
              block={true}
              propsHtml={{ id:"btn-create-user" , onClick:this.handlerClickToggle }}/>
          </div>
        }
        
        <br/>
        <br/>

        {
          this.state.show_create ?
            <FormDinamyc formProps={this.state.formCreateUser} /> 
          :

          <table className="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Email</th>
                <th scope="col">Option</th>
              </tr>
            </thead>
            <tbody>
              {
                this.state.users.map((user, key) =>
                <tr key={key}>
                  <th scope="row">{key}</th>
                  <td>{user.profile.name} {user.emails[0].address}</td>
                  <td>
                    <Button
                      text={'delete'}
                      classCustom={"btn-outline-danger"}
                      block={true}
                      propsHtml={{
                        id:user._id, 
                        onClick:this.handlerDelete 
                      }}/>
                  </td>
                </tr>
                )
              } 
            </tbody>
          </table>
        }

        <br />
        <br />
      </Container>
    )
  }
}

export default UsersSuperAdmin;