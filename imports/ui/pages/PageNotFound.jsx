import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

import '../styles/notfound.css';

class PageNotFound extends Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    document.body.style.backgroundColor = "#463164";
  }

  handlerClickGoHome = (event) => {
    event.preventDefault();
    document.body.style.backgroundColor = "#fff";
    FlowRouter.go('Index');
  }

  render() {
    return (
      <div className="bg-purple">
        <div className="stars">

          <div className="central-body">
            <img className="image-404" src="/images/notfound/404.svg" width="300px" />
            <a href="#" className="btn-go-home" onClick={this.handlerClickGoHome}>GO BACK HOME</a>
          </div>
          <div className="objects">
            <img className="object_rocket" src="/images/notfound/rocket.svg" width="40px" />
            <div className="earth-moon">
              <img className="object_earth" src="/images/notfound/earth.svg" width="100px" />
              <img className="object_moon" src="/images/notfound/moon.svg" width="80px" />
            </div>
            <div className="box_astronaut">
              <img className="object_astronaut" src="/images/notfound/astronaut.svg" width="140px" />
            </div>
          </div>
          <div className="glowing_stars">
            <div className="star" />
            <div className="star" />
            <div className="star" />
            <div className="star" />
            <div className="star" />
          </div>
        </div>
      </div>
    )
  }
}

export default PageNotFound;