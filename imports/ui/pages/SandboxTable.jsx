import React from 'react';

import { Tables, Button } from '../components/Index'


const table_config = {
  rowsPerPage: 20,
  loadMessage: 'Loading ...',
  seachField: true,
  seachFieldStyle: {marginBottom: 20},
  thead: 'thead-dark',
  tbody: 'table-group-divider',
  seachCustomClass: '',
  seachPlaceHolder: 'Seach ...',
  custom_class: 'table-dark table-striped-columns table-hover',
  fields: [
    {
      key: '_id',
      label: 'ID',
      custom_class: 'badge-pill badge-primary',
      scope_row: true
    },
    {
      key: 'name',
      label: 'Name',
    },
    {
      key: 'name',
      label: 'Name',
      hidden: true
    },
    {
      key: 'value',
      label: 'Total',
      fn: (total_order, object) => {
        return Number(total_order).toLocaleString();
      },
    },
    {
      key: 'createdAt',
      label: 'Date',
      fn: (createdAt, object) => {
        return new Date(createdAt).toLocaleString()
      },
      //sortOrder: 1, //FORCE ORIENTATION SORT
      sortDirection: 'descending' // descending or acending
    },
    {
      key: 'status',
      label: 'Status',
      fn: (status_order) => <span className={`badge badge-pill ${status_order == 'close'?'badge-danger':'badge-success'}`}>{status_order}</span>
    },
    {
      key: '_id',
      label: 'Options',
      fn: (_id, object) => 
        <div className='row'>
          <button className='col btn btn-info' style={{marginRight: 10}}>Create</button> 
          <button className='col btn btn-success' style={{marginRight: 10}}>Edit</button>
          <button className='col btn btn-danger' style={{marginRight: 10}}>Delete</button>
        </div>
    },
  ]
}

export default SandBoxTable = () => {
  return (
    <div className='container'>
      <Tables collection='find.options.sandbox_table' query={{}} data_struct={table_config} custom_class=''/>
      <br />
    </div>
  )
}
