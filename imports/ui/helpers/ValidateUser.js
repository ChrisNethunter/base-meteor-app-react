import { Meteor } from 'meteor/meteor';
import { AlertNotification } from './Alerts';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import { TAPi18n } from 'meteor/tap:i18n';

export default new class ValidateUserLogged {
  constructor() {

  }

  user_active(id) {
    if (Roles.userIsInRole(id, ['superadmin'])) {
      return true
    }
    if (!Meteor.user().emails[0].verified) {
      return false
    }
    return true
  }

  user_redirect() {
    if (Meteor.userId()) {
      setTimeout(() => {
        if (Roles.userIsInRole(Meteor.userId(), ['superadmin'])) {
          FlowRouter.go( "App.superadmin.users" )
        }else if(Roles.userIsInRole(Meteor.userId(), ['clients'])){
          FlowRouter.go( "App.superadmin.users" )
        }else{
          FlowRouter.go( "App" )
        }
      }, 500)
    }
  }

}
