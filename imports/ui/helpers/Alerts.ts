/**
 * 
 *  This class is in charge of generate alerst for all system
 *  @class Alert
 *  type_icons https://getbootstrap.com/docs/3.3/components/
 */
interface objetAlert {
  type: string,
  icon: string,
  title: string,
  message: string
}

/*
    objetAlert : {
        type : 'success' required,
        icon : 'fab fab-example' optional,
        title : 'example', optional,
        message : 'Message example' required
    }
*/
export default class Alert {
  public time: number;
  public animation: string;
  public type_alert: object;

  constructor() {
    this.time = 4000;
    this.animation = '';
    this.type_alert = {
      success: 'alert-success',
      warning: 'alert-warning',
      info: 'alert-info',
      danger: 'alert-danger',
      dark: 'alert-dark',
      light: 'alert-light'
    }
  }

  notifyAlert(object: objetAlert) {

    if (!object.type) {
      this.notifyAlert({
        type: 'danger',
        icon: null,
        title: 'Error type alert',
        message: 'Undefined type alert notifyAlert() function'
      });
      return false
    }
    Bert.alert(object.message, object.type, 'fixed-top', object.icon);

    /* this.removeAllAlerts();
    let alert = this.typeAlert(object.type , object.icon ? object.icon : null , object.title, object.message );

    //CREATE DIV CONTENT ALERT
    let theParent = document.body;
    let theKid = document.createElement("div");
    theKid.setAttribute("id", "alerts-wrapper");
    theKid.setAttribute("style", "position:fixed;z-index:9999 !important;width: 100%;");
    //CREATE DIV CONTENT ALERT

    //ADD DIV ALERT
    theKid.innerHTML += alert
    theParent.insertBefore(theKid , theParent.firstChild);
    //ADD DIV ALERT

    setTimeout( ()=>{
        document.getElementById('target-alert').classList.add('show');
    },100);
    document.getElementById('close-alert').addEventListener('click' , ( event ) =>{
        event.preventDefault();
        document.getElementById('target-alert').classList.remove('show');
        setTimeout( ()=>{
            this.removeAllAlerts();
        },100);
    });

    setTimeout( ()=>{
        if(document.getElementById('target-alert')){
            document.getElementById('target-alert').classList.remove('show');
        }
    },this.time); */
  }

  removeAllAlerts() {
    if (document.getElementById('alerts-wrapper')) {
      document.getElementById('alerts-wrapper').remove();
    }
  }

  typeAlert(type: string, icon: string, title: string, message: string) {
    icon = icon ? `<i class="${icon}"></i>` : '';
    return `<div id='target-alert' class="alert ${this.type_alert[type]} alert-dismissible fade">
                    <button id='close-alert' type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    ${title ? `<h4 class="alert-heading"> ${icon}  ${title} </h4>` : `<br>`}
                    <p>
                        ${!title ? `${icon}` : ``} <span>${message}</span>
                    </p>
                </div>`;
  }

}

export const AlertNotification = new Alert();

