export default class FetchRequest {

  postJSON<Req, Res>(url: string, data: object, headers: object, origin: string) {

    let setHeaders = this.setHeaders(headers, origin);
    let context = this;

    return new Promise<any>((resolve, reject) => {
      fetch(url, {
        headers: setHeaders,
        credentials: 'same-origin',
        method: "POST",
        body: JSON.stringify(data)
      }).then((response) => {
        response.json().then((res) => {
          resolve(this.responseFetch(res))
        }).catch((res) => {
          reject(res);
        });
      }).catch((err) => {
        reject(err);
      });

    });
  }

  getJSON<Req, Res>(url: string, origin: string) {

    let headers = {
      //'baulter-access-token': LocalStorage.getValue("auth_token"),
      'access-method': 'api-json'
    }
    let setHeaders = this.setHeaders(headers, origin);
    let context = this;

    return new Promise(function (resolve, reject) {
      fetch(url, {
        headers: setHeaders,
        credentials: "same-origin"
      }).then(function (response) {
        response.json().then(function (res) {
          resolve(context.responseFetch(res))
        }).catch(function (res) {
          reject(res);
        });
      });
    });
  }

  setHeaders(headers: object, origin: string) {

    const requestHeaders: HeadersInit = new Headers();
    requestHeaders.set('Content-Type', 'application/json');
    requestHeaders.set('access-method', 'api-json');
    requestHeaders.set('Origin', origin);

    if (headers) {
      for (let header in headers) {
        requestHeaders.set(header, headers[header]);
      }
    }

    return requestHeaders
  }

  responseFetch(res: any) {
    if (res && res.success) {
      return {
        success: true,
        data: res
      }
    } else {
      return {
        success: false,
        data: res
      }
    }
  }
}



export const Request = new FetchRequest(); 