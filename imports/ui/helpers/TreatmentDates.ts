
export default class DateControl {
  constructor() {

  }

  treatmentDateStandard(date: Date) {
    let dateFormat = new Date(date).toISOString().slice(0, 19).replace('T', " ");
    return dateFormat.split(' ')[0];
  }


}

export const TreatmentDates = new DateControl();