import { AlertNotification } from './Alerts';
/*  Ejemplos para llamar la clase
    let target = event.currentTarget;
    let validateForm = Validations.validateForm(target.elements); */
export default class ValidationsElements {
  public validations: object;
  public class_valid: string;
  public class_invalid: string;

  constructor() {
    this.validations = {
      number: 'validateNumber',
      url: 'validateUrl',
      email: 'validateEmail',
      text: 'validateEmpty',
      password: 'validatePassword',
      select: 'validateEmpty',
      time: 'validateTime',
      date: 'validateDate',
      color: 'validateEmpty',
      checkbox: 'validateChecked'
    }
    this.class_invalid = 'is-invalid';
    this.class_valid = 'is-valid';

  }

  validateForm(target: Array<any>) {
    for (let element of target) {
      if (element.type !== 'submit') {
        this.resetStylealidateError(element);
        let targetToValidate = element.value;

        if (element.getAttribute('required-regex') == 'true') {

          if (element.type == 'checkbox') {
            targetToValidate = element;
            if (!element.checked) {
              this.alertValidation(element);
              return false
            }
          } else {
            if (element.value === '') {
              this.styleValidateError(element);
              this.alertValidation(element);
              return false
            }

            if (this.validations[element.type]) {
              let execute_validation = eval(`this.${this.validations[element.type]}('${targetToValidate}')`);
              if (!execute_validation) {
                this.styleValidateError(element);
                this.alertValidation(element);
                return false
              }
            }

          }

        }

      }
    }
    return {
      status: true
    }
  }

  alertValidation(element: any) {
    AlertNotification.notifyAlert({
      type: 'danger',
      icon: 'ni ni-fat-remove',
      message: element.getAttribute('data-message-validation')
    });
  }

  getDataForm(target: Array<any>) {
    let data_form: object = {};
    for (let element of target) {
      if (element.type !== 'submit') {
        if (element.type == 'checkbox') {
          data_form[element.id] = element.checked;
        } else {
          data_form[element.id] = element.value;
        }

      }
    }
    return data_form
  }

  styleValidateError(element: any) {
    element.classList.add(this.class_invalid);
  }

  styleValidateSucces(element: any) {
    element.classList.add(this.class_valid);
  }

  resetStylealidateError(element: any) {
    element.classList.remove(this.class_invalid);
  }

  validateEmpty(element: string) {
    if (element === '') {
      return false
    } else {
      return true
    }
  }

  validateNumber(element: number) {
    if (isNaN(element)) {
      return false
    } else {
      return true
    }
  }

  validateEmail(email: string) {
    const expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (expr.test(email)) {
      return true
    } else {
      return false
    }
  }

  validateUrl(url: string) {
    const expr = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    if (expr.test(url)) {
      return true;
    } else {
      return false;
    }
  }

  validateCheck(checkhtml: any) {
    if (checkhtml.checked) {
      return true
    } else {
      return false
    }
  }

  validateTime(time: string) {
    const expr = /\d+:\d+/;
    if (expr.test(time)) {
      return true
    } else {
      return false
    }
  }

  validateDate(date: string) {
    var matches = /^(\d{1,2})[-\/](\d{1,2})[-\/](\d{4})$/.exec(date);
    if (matches == null) {
      return false;
    } else {
      return true;
    }
  }

  validatePassword(pass: string) {
    if (pass) {
      return true
    } else {
      return false
    }
  }

  validateChecked(check: any) {

    if (check.checked) {
      return true
    } else {
      return false
    }
  }


}

export const Validations = new ValidationsElements(); 