import React from 'react';
import { useTranslation, withTranslation } from 'react-i18next';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
class MainLayout extends (React.Component) {
  constructor(props) {
    super(props);
    this.state = {
      t: "",
      i18n: ""
    }
  }

  componentDidMount() {
    this.assingTraslation()
  }

  assingTraslation() {
    this.setState({
      t: this.props.t,
      i18n: this.props.i18n
    })
  }

  changeLanguage = (lng) => {
    this.state.i18n.changeLanguage(lng);
    let route = FlowRouter.getRouteName();
    /* if(route == "App.home"){
        location.reload()
    } */
    location.reload()
  }

  render() {
    const { t, i18n } = this.props;
    const changeLanguage = lng => {
      i18n.changeLanguage(lng);
      if (route == "App.home") {
        location.reload()
      }
    };
    return (
      <div>
        {React.createElement(this.props.page, {
          traslation: this.props.t,
          changeLanguage: changeLanguage
        }, null)}
      </div>
    );
  }
}

export default withTranslation()(MainLayout);