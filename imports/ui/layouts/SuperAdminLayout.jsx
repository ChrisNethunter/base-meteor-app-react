import React from 'react';
import { useTranslation, withTranslation } from 'react-i18next';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

import { 
  Header,
  Sidebar,
  Toolbar
} from '../components/Index';

import { LocalStorage } from '../helpers/LocalStorage';

class SuperAdminLayout extends (React.Component) {
  constructor(props) {
    super(props);
    this.state = {
      t: "",
      i18n: "",
      expand:false,
    }
  }

  componentDidMount() {
    this.assingTraslation()
    this.setExpand();
  }

  setExpand(){
    let status =  LocalStorage.getValue('expand')
    if(status){
      this.setState({expand : status})
    }
  }

  assingTraslation() {
    this.setState({
      t: this.props.t,
      i18n: this.props.i18n
    })
  }

  changeLanguage = (lng) => {
    this.state.i18n.changeLanguage(lng);
    let route = FlowRouter.getRouteName();
    /* if(route == "App.home"){
        location.reload()
    } */
    location.reload()
  }

  expand_content = (event) => {
    event.preventDefault();
    this.setState({expand : !this.state.expand})
    LocalStorage.setValue('expand',!this.state.expand)
  }

  render() {
    const { t, i18n } = this.props;
    const changeLanguage = lng => {
      i18n.changeLanguage(lng);
      if (route == "App.home") {
        location.reload()
      }
    };
    return (
      <div>
       <div>
        <Header 
          expand={this.expand_content} 
          status_expand={this.state.expand}/>

        <div className="container-fluid">
          <div className="row">
            {
              this.state.expand ?
                ""
              :
                <Sidebar/>
            }
            <main className={ `${this.state.expand ? "col-md-12 col-lg-12":"col-md-9 col-lg-10" } ms-sm-auto  px-md-4` }>
              {
                React.createElement(this.props.page, {
                  traslation: this.props.t,
                  changeLanguage: changeLanguage
                }, null)
              } 
            </main>
          </div>
        </div>
      </div>
     
      </div>
    );
  }
}

export default withTranslation()(SuperAdminLayout);