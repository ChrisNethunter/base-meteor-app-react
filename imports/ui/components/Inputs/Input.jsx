
import React from 'react';

const Input = ({label, classCustom , propsHtml , options}) => {
	let classDefault = `form-control`;
	let renderInput;

	if(propsHtml.type == 'checkbox'){
		classDefault = 'form-check-input'
	}
	
	if(propsHtml.type == 'select'){
		renderInput = <InputSelect  propsHtml={propsHtml} options={options} />
	}else{
		renderInput = <input  className={`${classDefault} ${classCustom}`} {...propsHtml}/>;
	}

	return (
		<div className="form-group">
			<label style={{marginBottom:10}}>
				<b>{label}</b>
			</label>
			<br />
			{ renderInput }
			<br />
		</div>
	);
}

const InputSelect = ({ propsHtml , options }) =>{
	return (
		<select className="form-select" {...propsHtml}>
			{
				options.map((option, optionkey) =>
					<option key={optionkey} value={option._id}>{option.name}</option>
				)
			}
		</select>
	)
}

Input.defaultProps = {
	custom:"test",
	type:"text",
	label:"label",
	classCustom:'',
	placeholder:''
}

export default Input;