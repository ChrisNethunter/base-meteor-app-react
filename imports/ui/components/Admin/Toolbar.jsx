import React, { useState } from 'react';

function Toolbar({ name , tools }) {
  return (
    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <h1 className="h2">{name}</h1>
      <div className="btn-toolbar mb-2 mb-md-0">
        <div className="btn-group me-2">
          <button type="button" className="btn btn-sm btn-outline-primary">Share</button>
          <button type="button" className="btn btn-sm btn-outline-success">Export</button>
        </div>
        <button type="button" className="btn btn-sm btn-outline-warning dropdown-toggle">
          <span data-feather="calendar" className="align-text-bottom" />
          This week
        </button>
      </div>
    </div>
  );
}
export default Toolbar;  