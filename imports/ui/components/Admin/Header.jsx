import React, { useState , useEffect } from 'react';
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

function Header({expand , status_expand}) {
  
  useEffect(() => {
  },[status_expand]);

  const handlerClickSesionOut = (event) => {
    event.preventDefault();
    Meteor.logout((err) => {
      if (!err) {
        localStorage.clear();
        FlowRouter.go('Index');
      }
    });
  }

  return (
    <header className="navbar navbar-dark sticky-top flex-md-nowrap p-0 ">
      <a 
        className="navbar-brand col-md-3 col-lg-2 me-0 px-3 fs-6 " 
        href="/" onClick={expand}>
        <b className={!status_expand ? 'text-white hvr-grow' : 'text-black hvr-grow'}>
          { !status_expand ? <i className="fas fa-arrow-left"></i> : <i className="fas fa-arrow-right"></i> } {' '}Admin template 
        </b>
      </a>
      <button 
        className="navbar-toggler position-absolute d-md-none collapsed tex-black" 
        type="button" 
        data-bs-toggle="collapse" 
        data-bs-target="#sidebarMenu" 
        aria-controls="sidebarMenu" 
        aria-expanded="false" 
        aria-label="Toggle navigation">
        <span className="navbar-toggler-icon" />
      </button>
      <div className="navbar-nav w-100"></div>
      {/* <input className="form-control form-control-dark w-100 rounded-0 border-0" type="text" placeholder="Search" aria-label="Search" />  */}
      <div className="navbar-nav">
        <div className="nav-item text-nowrap">
          <a className="nav-link px-3 text-black hvr-grow title-out" href="#" onClick={handlerClickSesionOut}> 
            <i className="fas fa-sign-out-alt"></i> Sign out
          </a>
        </div>
      </div>
    </header>
  );
}
export default Header;  