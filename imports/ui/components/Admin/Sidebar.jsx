import React, { useState } from 'react';  
  
function Sidebar() {  
  return (  
    <nav id="sidebarMenu" className="col-md-2 col-lg-2 d-md-block bg-dark sidebar collapse">
      <div className="position-sticky pt-3 sidebar-sticky">
        <ul className="nav flex-column" >
          <Item name="Users" route="/superadmin/users" icon="fas fa-user"/>
          <Item name="Stores" route="/superadmin/users" icon="fas fa-store"/>
        </ul>
      </div>
    </nav>
  );  
} 

function Item({ name , route , icon }){
  return (
    <li className="nav-item hvr-grow">
      <a className="nav-link text-light active" aria-current="page" href={route}>
        <span data-feather="home" className="align-text-bottom" />
        <i className={icon ? icon:"fas fa-question"}></i>  { name }
      </a>
    </li>
  )
}

export default Sidebar;  