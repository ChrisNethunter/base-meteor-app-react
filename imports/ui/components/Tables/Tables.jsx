import { Meteor } from 'meteor/meteor';
import React, { useState, useEffect } from 'react';
import { Input } from '../Index';

const LIST_REQUIRED_PARAMETERS = ['rowsPerPage', 'fields', 'seachField', 'loadMessage']

const verify_collection_config = (collection, data_struct) => {
  // VERIFICATION PARAMETERS CONFIG
  let testing_data = collection[0]
  for (let key_validation of data_struct.fields) {
    let verity_key = key_validation.key in testing_data
    if (verity_key < 0) {
      return false
    }
  }
  return true
}

const verify_parameters_config = data_struct => {
  if (!Object.keys(data_struct).length > 0) {
    return [false, 'Required data_struct is required']
  } else {
    //PARAMETERS VERIFICATION
    for (let keys of Object.keys(data_struct)) {
      let verity_key = LIST_REQUIRED_PARAMETERS.findIndex((item, key) => key == keys)
      if (verity_key < 0) {
        return [false, 'Required parameters not enougth for data_struct']
      }
    }

    //FIELDS VERIFICATION
    for (let fileds of data_struct.fields) {
      if (!'key' in fileds) return [false, 'Required key in fields data_struct']
      if (!typeof (fileds.key) == 'string') return [false, 'Required field.key must to be string']
      if (!'label' in fileds) return [false, 'Required label in fields data_struct']
      if (!typeof (fileds.label) == 'string') return [false, 'Required field.label must to be string']
      if (!'fn' in fileds) return [false, 'Required fn in fields data_struct']
      if (!typeof (fileds.fn) == 'function') return [false, 'Required field.fn must to be string']
    }
  }
}

const filter_hidden_parameters = item => {
  if ('hidden' in item) {
    if (item.hidden == true) {
      return false
    } else {
      return true
    }
  } else {
    return true
  }
}


const Tables = ({ collection, query = {}, data_struct, custom_class }) => {
  const [dataTable, setDataTable] = useState([]);
  const [tableIssues, setTableIssues] = useState(false);
  const [loadingData, setLoadingData] = useState(false);
  const [pageIndex, setPageIndex] = useState(0);  
  const [numberPages, setNumberPages] = useState(1);

  useEffect(() => {
    if (loadingData == false) {
      //PARAMETERS VERIFICATION
      let verification_data_struct = verify_parameters_config(data_struct)
      if (!verification_data_struct[0]) {
        setTableIssues(true)
        new Meteor.Error(verification_data_struct[1]);
      }

      //LOAD DATA TABLE
      getTableData(query)
    }
  }, [loadingData, dataTable]);

  useEffect(() => {
    if (loadingData == true) {
      //LOAD DATA TABLE
      getTableData(query)
    }
  }, [pageIndex]);

  getTableData = (query = {}) => {
    let options = { limit: data_struct.rowsPerPage, skip : pageIndex }
    let key_to_sort = data_struct.fields.findIndex((key_item, key) => 'sortDirection' in key_item)
    if (key_to_sort > 0) {
      let sort = {}
      // NATURAL SORT
      sort[data_struct.fields[key_to_sort].key] = data_struct.fields[key_to_sort].sortDirection == 'descending' ? -1 : 1
      // FORCED SORT
      if ('sortOrder' in data_struct.fields[key_to_sort]) {
        sort[data_struct.fields[key_to_sort].key] = data_struct.fields[key_to_sort].sortOrder
      }
      options['sort'] = sort
    }

    Meteor.call(collection, query, options, (error, response) => {
      if (!error) {
        if (response.data.length > 0) {
          //VERIFY THE INPUT PARAMETERS ACORD TO THE COLLECTION
          if (verify_collection_config(response.data, data_struct)) {
            setNumberPages(response.count_data / data_struct.rowsPerPage)
            setDataTable(response.data)
            setLoadingData(true)
          } else {
            setTableIssues(true)
            new Meteor.Error("The parameters is not in collection");
          }
        }
      }
    })
  }

  HandlerEventSearch = event => {
    filter = query
    var mongoDbArr = [];

    for (let item of data_struct.fields) {
      let segment = {}
      segment[item.key] = new RegExp(event.currentTarget.value)
      mongoDbArr.push(segment);
    }

    filter['$or'] = mongoDbArr
    getTableData(filter)
  }

  getIndexPaginationString = key => {
    return key == 0 ?
      pageIndex: 
    key == 1? 
      pageIndex + 1: 
    key == 2?
      pageIndex + 2:
    0
  }

  getIndexPagination = key => {
    if (key > 1){
      return pageIndex + 1 == key
    }
  }

  return (
    <>
      {loadingData ?
        tableIssues ?
          <>
            { // SEACH FIELD
              data_struct.seachField ?
                <div style={{...data_struct.seachFieldStyle}}>
                  <input className={`form-control ${data_struct.seachCustomClass}`} placeholder={data_struct.seachPlaceHolder} onKeyDown={HandlerEventSearch} />
                </div>
                : ''
            }

            <table className={`table ${data_struct.custom_class}`}>
              <thead className={`${data_struct.thead}`}>
                <tr>
                  {data_struct.fields.filter(filter_hidden_parameters).map((item, key) => (
                    <th
                      key={key}
                      scope="col"
                      className={'custom_class' in item ? item.custom_class : ''}>
                      {item.label}
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody className={`${data_struct.tbody}`}>
                {dataTable.map((object_ob, key_ob) => (
                  <tr key={key_ob}>
                    <>
                      {data_struct.fields.filter(filter_hidden_parameters).map((key_item, key) => (
                        <td key={key}>
                          {
                            'fn' in key_item ? // CUSTOM ITEM FOR PARAMETER
                              key_item.fn(object_ob[key_item.key], object_ob)
                            :
                              <p // OTHER ITEM NOT CUSTOM SETTINGS
                                className={'custom_class' in key_item ? key_item.custom_class : ''}>
                                {`${object_ob[key_item.key]}`}
                              </p>
                          }
                        </td>
                      ))}
                    </>
                  </tr>
                ))}
              </tbody>
            </table>
            <nav>
              <ul className="pagination pagination-sm justify-content-center">
                <li className={`page-item ${pageIndex == 1? 'disabled':''}`} onClick={() => setPageIndex(pageIndex - 1)}>
                  <a className="page-link text-dark">Previous</a>
                </li>
                {
                  numberPages > pageIndex && pageIndex != 0?
                  <li  onClick={() => setPageIndex(0)} className={`page-item`}>
                    <a className="page-link text-dark">...</a>
                  </li>
                  :''
                }
                { 
                  new Array(numberPages > 3? 3 : numberPages ).fill({key: 1})
                  .map((item, key) => (
                    <div key={key}>
                      {
                        getIndexPaginationString(key) -1 < numberPages && getIndexPaginationString(key) > 0 ?
                        <li onClick={() => setPageIndex(getIndexPaginationString(key) -1)} className={`page-item ${pageIndex == getIndexPaginationString(key) -1? 'active' : ''}`}>
                          <a className="page-link text-dark">{getIndexPaginationString(key)}</a>
                        </li>
                        :''
                      }
                    </div>
                  )) 
                }
                 {
                  numberPages > pageIndex && pageIndex != numberPages?
                  <li  onClick={() => setPageIndex(numberPages)} className={`page-item`}>
                    <a className="page-link text-dark">...</a>
                  </li>
                  :''
                }
                <li className={`page-item ${pageIndex == numberPages? 'disabled':''}`} onClick={() => setPageIndex(pageIndex + 1)}>
                  <a  className="page-link text-dark">Next</a>
                </li>
              </ul>
            </nav>
          </>
          :
          <h3 style={{ color: 'red' }}>Error Table</h3>
        :
        <h3>{data_struct.loadMessage}</h3>
      }
    </>
  );
}

export default Tables;