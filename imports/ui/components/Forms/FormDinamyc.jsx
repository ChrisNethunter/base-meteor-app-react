import React from 'react';
import { Input , Button } from '../Index';


let options_select_test = [
  {
    name: 'name 1',
  },
  {
    name: 'name 4',
  },
  {
    name: 'name 3',
  }
]

let example_create_form = {
  formCreateTest: {
    title: 'Form Test ',
    rows: true,
    propsHtml: {
      'id': 'form-test',
      'onSubmit': this.handlerSubmitFormTest,
      'className': 'my-class-form'
    },
    buttons: {
      buttonSubmit: {
        text: 'ENVIAR',
        propsHtml: {
          className: 'btn btn-primary btn-lg btn-block'
        }
      },
      buttonCancel: {
        text: 'CANCELAR',
        propsHtml: {
          onClick: this.handlerClickCancel,
          className: 'btn btn-secondary btn-lg btn-block'
        }
      }
    },
    inputs: [
      {
        label: 'Test',
        cols: {
          md: 6
        },
        propsHtml: {
          'id': 'test',
          'data-validate': 'is-necessary',
          'data-message-validation': 'message invalid type',
          'required': false,
          'required-regex': 'true',
          'type': 'text',
          'onKeyUp': this.handlerKeyUp,
          'className': 'myclass'
        }
      },
      {
        label: 'Test',
        cols: {
          md: 6
        },
        propsHtml: {
          'id': 'test7',
          'data-validate': 'is-necessary',
          'data-message-validation': 'message invalid type',
          'required': false,
          'required-regex': 'true',
          'type': 'text',
          'onKeyUp': this.handlerKeyUp,
          'className': 'myclass'
        }
      },

      {
        label: 'Test',
        cols: {
          md: 3
        },
        propsHtml: {
          'id': 'test8',
          'data-validate': 'is-necessary',
          'data-message-validation': 'message invalid type',
          'required': false,
          'required-regex': 'true',
          'type': 'text',
          'onKeyUp': this.handlerKeyUp,
          'className': 'myclass'
        }
      },
      {
        label: 'Test 2',
        cols: {
          md: 3
        },
        propsHtml: {
          'id': 'test2',
          'data-validate': 'is-necessary',
          'data-message-validation': 'message invalid type',
          'required': false,
          'required-regex': 'true',
          'type': 'number',
          'onKeyUp': this.handlerKeyUp,
          'className': 'myclass'
        }
      },
      {
        label: 'Select Test 3',
        cols: {
          md: 3
        },
        propsHtml: {
          'id': 'test3',
          'data-validate': 'is-necessary',
          'data-message-validation': 'message invalid type',
          'required': false,
          'required-regex': 'true',
          'type': 'select',
          'onKeyUp': this.handlerChangeSelect,
          'className': 'myclass'
        },
        options: options_select_test
      },
      {
        label: 'Color test',
        cols: {
          md: 3
        },
        propsHtml: {
          'id': 'test4',
          'data-validate': 'is-necessary',
          'data-message-validation': 'message invalid type',
          'required': false,
          'required-regex': 'false',
          'type': 'color',
          'onKeyUp': this.handlerKeyUp,
          'className': 'myclass'
        }
      },
      {
        label: 'Checked test',
        propsHtml: {
          'id': 'test5',
          'data-validate': 'is-necessary',
          'data-message-validation': 'message invalid type',
          'required': false,
          'required-regex': 'true',
          'type': 'checkbox',
          'onKeyUp': this.handlerKeyUp,
          'className': 'myclass'
        }
      },
    ]
  },

}

//smart component
class FormDinamyc extends (React.Component) {
  constructor(props) {
    super(props);
    this.state = {
      inputs: [],
      idForm: '',
    }
  }

  renderInput = (inputelement, key) => {
    let default_class = 'container-input';
    if (this.props.formProps.rows) {
      for (let col in inputelement.cols ) {
        default_class = `${default_class} col-${col}-${inputelement.cols[col]}`
      }
    }
    return (
      <div className={default_class} key={key} >
        <Input
          key={key} 
          label={inputelement.label}
          propsHtml={inputelement.propsHtml}
          options={inputelement.options ? inputelement.options : null  }
        />
      </div>
    )
  }

  render() {
    return (
      <div>
        <h1> {this.props.formProps.title} </h1>
        <br></br>
        <form  {...this.props.formProps.propsHtml}>
          {this.props.formProps.rows ?
            <div className='row'>
              {
                this.props.formProps.inputs.map((inputelement, key) =>
                  this.renderInput(inputelement, key)
                )
              }
            </div>
            :
            this.props.formProps.inputs.map((inputelement, key) =>
              this.renderInput(inputelement, key)
            )
          }
          <br></br>
          <div className='row'>
            <div className='col-xl-6 col-md-6 col-sm-12 spacing-bottom'>
              <Button
                propsHtml={this.props.formProps.buttons.buttonSubmit.propsHtml}
                text={this.props.formProps.buttons.buttonSubmit.text}
                block={true}
              />
            </div>
            {
              this.props.formProps.buttons.buttonCancel ?
                <div className='col-xl-6 col-md-6 col-sm-12'>
                  <Button
                    propsHtml={this.props.formProps.buttons.buttonCancel.propsHtml}
                    text={this.props.formProps.buttons.buttonCancel.text}
                    block={true}
                  />
                </div>
                :
                ""
            }
          </div>
        </form>
      </div>
    );
  }
}

export default FormDinamyc