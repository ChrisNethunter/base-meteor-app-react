import React from 'react';

const Button = ({text, classCustom , propsHtml , block=false }) => {

	if( block ){
		return(
			<div className="d-grid gap-2">
				<button 
					className={`btn ${classCustom}`}
					{...propsHtml}>
					<b>{text}</b>
				</button>
			</div>
		)
	}else{
		return (
			<button 
				className={`btn ${classCustom}`}
				{...propsHtml}>
				<b>{text}</b>
			</button>
		);
	}
}

Button.defaultProps = {
	propsHtml:"example class",
	text:"text button",
	classCustom:'btn-primary'
}

export default Button;