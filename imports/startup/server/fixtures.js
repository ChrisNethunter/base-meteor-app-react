import { Meteor } from 'meteor/meteor';
import { Migrations } from '../../api/Migrations/migrations';
import { sandbox_table } from '../../api/sandbox_table/sandbox_table';

const random = (length = 8) => {
  return Math.random().toString(16).substr(2, length);
};

Meteor.startup(() => {

  if (!Migrations.findOne({ slug: 'users4' })) {
    Roles.createRole('superadmin');
    var users = [
      { name: "General Admin", email: "admin@meteor.com", pass: "admin@2020", roles: ['superadmin'] },
    ];

    for (user of users) {
      id = Accounts.createUser({
        email: user.email,
        password: user.pass,
        profile: {
          name: user.name,
          status: true,
        }
      });

      if (user.roles.length > 0) {
        // Need _id of existing user record so this call must come
        // after `Accounts.createUser` or `Accounts.onCreate`
        Roles.addUsersToRoles(id, user.roles);
      }

      Meteor.users.update({ _id: id }, {
        $set: {
          'services.email.verificationTokens': [],
          'emails.0.verified': true
        }
      });
    }
    console.log('default users...')
    Migrations.insert({ slug: 'users4' });
  }

  if (!Migrations.findOne({ slug: 'standard' })) {
    Roles.createRole('standard');

    console.log('default standard...')
    Migrations.insert({ slug: 'standard' });
  }


  if (!Migrations.findOne({ slug: 'sandbox' })) {

    for (let i =0; i< 20000; i++){
      let date = new Date()
      date.setDate(date.getDate() - i)
      date.setHours(date.getHours() - i)

      let rand = Math.random().toString().substr(2, 8);

      let object = { 
        name: `${random(12)}`,
        value: rand,
        createdAt: date,
        status: i %2 == 0 ? 'open' : 'close'
      }

      sandbox_table.insert(object)

    }
    console.log('default sandbox...')
    Migrations.insert({ slug: 'sandbox' });
  }

});
