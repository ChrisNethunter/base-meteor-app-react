import { Meteor } from 'meteor/meteor';

Accounts.config({
  sendVerificationEmail: true,
  forbidClientAccountCreation: false,
});


process.env.MAIL_URL = Meteor.settings.MAIL_URL;

