
import schema_email from './templates_emails/verification_email';
import schema_informative_email from './templates_emails/informative_email';

export default class EmailSend {
  constructor() {

  }

  sendVerification(id_user: string) {
    Meteor.setTimeout(() => {
      Accounts.emailTemplates.from = "Meteor Settings";
      Accounts.emailTemplates.verifyEmail.subject = function (user) {
        return 'SITE NAME';
      };

      Accounts.emailTemplates.verifyEmail.html = (user, url) => {
        return schema_email(user, url)
      };
      Accounts.sendVerificationEmail(id_user);
    }, 2 * 1000);
  }

  sendMessageSubject(email: string, from: string, subject: string, text: string) {
    Meteor.setTimeout(() => {
      Email.send({
        to: email,
        from: from,
        subject: subject,
        text: text,
        html: schema_informative_email(email, text)
      });
    }, 2 * 1000);
  }

}


export const SendEmail = new EmailSend(); 