import logger from 'node-color-log';

export default new class LoggerDebug {
  constructor() {

  }

  success(data) {
    logger.success(data)
  }

  error(data) {
    logger.error(data)
  }

  warning(data) {
    logger.warn(data)
  }

  info(data) {
    logger.info(data)
  }

}
