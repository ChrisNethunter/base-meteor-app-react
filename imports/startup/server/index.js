
import './fixtures.js';
import './register-api.js';
import './accounts.js';
import './renderserver.js';
import './prerender.js';
import './sitemap.js';