import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import React from 'react';
import { mount, withOptions } from 'react-mounter';
import { Seo } from '../../ui/helpers/Seo';

//Layouts
import MainLayout from '../../ui/layouts/MainLayout';
import SuperAdminLayout from '../../ui/layouts/SuperAdminLayout';
//Layouts

//Auth
import Login from '../../ui/pages/Auth/Login';
import Signup from '../../ui/pages/Auth/Signup';
import Forgot from '../../ui/pages/Auth/Forgot';
//Auth

//Administrator
import App from '../../ui/App';
//Administrator

//SuperAdmin
import UsersSuperAdmin from '../../ui/pages/Dashboard/Superadmin/UsersSuperAdmin';
//SuperAdmin


//NOTFOUND
import PageNotFound from '../../ui/pages/PageNotFound';


import SandBoxTable from '../../ui/pages/SandboxTable';

FlowRouter.route('/', {
  name: 'Index',
  action() {
    Seo.titleTab = 'Home';
    mount(MainLayout, { page: Login });
  }
});

FlowRouter.route('/app', {
  name: 'App',
  action() {
    Seo.titleTab = 'Start';
    mount(MainLayout, { page: App });
  }
});

FlowRouter.route('/login', {
  name: 'App.login',
  action() {
    Seo.titleTab = 'Login';
    mount(MainLayout, { page: Login });
  }
});

FlowRouter.route('/signup', {
  name: 'App.signup',
  action() {
    Seo.titleTab = 'Signup';
    mount(MainLayout, { page: Signup });
  }
});

FlowRouter.route('/sandbox_table', {
  name: 'App.sandbox_table',
  action() {
    Seo.titleTab = 'SandboxTable';
    mount(MainLayout, { page: SandBoxTable  });
  }
});

FlowRouter.route('/forgotpassword', {
  name: 'App.forgot_password',
  action() {
    Seo.titleTab = 'Signup';
    mount(MainLayout, { page: Forgot });
  }
});

//SuperAdmin
FlowRouter.route('/superadmin/users', {
  name: 'App.superadmin.users',
  action() {
    Seo.titleTab = 'Start';
    mount(SuperAdminLayout, { page: UsersSuperAdmin });
  }
});
//SuperAdmin

/* ADMIN */
FlowRouter.route('*', {
  action() {
    Seo.titleTab('Not found');
    mount(MainLayout, { page: PageNotFound });
  },
});
