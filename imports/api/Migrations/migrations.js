import { Mongo } from 'meteor/mongo';

export const Migrations = new Mongo.Collection('migrations');
