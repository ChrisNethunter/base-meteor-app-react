import { Meteor } from 'meteor/meteor';
import { sandbox_table } from '../sandbox_table';

Meteor.publish("all.sandbox_table", function () {
    return sandbox_table.find()
})