import { Meteor } from 'meteor/meteor';
import ValidationsSchemas  from '../../startup/server/helpers/ValidationsSchemas';
import { Schema } from './schema';
import { sandbox_table } from './sandbox_table';


Meteor.methods({
    'create.sandbox_table'(data){
        if (! Meteor.userId()) throw new Meteor.Error("not-authorized");
        let validate_schema =  new ValidationsSchemas(Schema);
        let schema = validate_schema.validate(data);
        
        if( schema.status !== true ){
            throw new Meteor.Error(schema.data); 
        }

        return sandbox_table.insert(schema.data) 
    },

    'edit.sandbox_table'(id, data){
        if (! Meteor.userId()) throw new Meteor.Error("not-authorized");
        let last_object = sandbox_table.findOne({_id: id})
        delete last_object['_id']

        for (let key in last_object){
            if (data.hasOwnProperty(key)){
                last_object[key] = data[key]
            }
        }

        let validate_schema =  new ValidationsSchemas(Schema);
        let schema = validate_schema.validate(last_object);
        
        if( schema.status !== true ){
            throw new Meteor.Error(schema.data); 
        }

        return sandbox_table.update({_id: id}, {$set: data}) 
    },

    'remove.sandbox_table'(id){
        if (! Meteor.userId()) throw new Meteor.Error("not-authorized");
        return sandbox_table.remove({_id: id}) 
    },
    
    'find.sandbox_table'(query){
        if (! Meteor.userId()) throw new Meteor.Error("not-authorized");
        return sandbox_table.find(query).fetch()
    },

    'find.options.sandbox_table'(query, options){
        let pagination_info = sandbox_table.find(query).count()
        let data = sandbox_table.find(query, options).fetch()
        return {data: data, count_data: pagination_info}
    },
    
    'findOne.sandbox_table'(query){
        if (! Meteor.userId()) throw new Meteor.Error("not-authorized");
        return sandbox_table.findOne(query) 
    },                  
});