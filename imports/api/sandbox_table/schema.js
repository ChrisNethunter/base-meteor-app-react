import SimpleSchema from 'simpl-schema';

export const Schema = new SimpleSchema({
    name: {
        type: String,
    },
    value: {
        type: Number,
    },
    createdAt: {
        type: Date,
    },
    status: {
        type: String,
    },
});