import SimpleSchema from 'simpl-schema';


export const user_schema = new SimpleSchema({
    email: {
        type: String,
    },
    password: {
        type: String,
    },
    profile: {
        type : Object,
        optional: true,
        defaultValue : {
            name : '',
            update_data : true,
            status : true 
        }
    },
    'profile.name': {
        type : String
    },
    'profile.update_data': { 
        type: Boolean,
        optional: true,
        defaultValue: true,
    },
    'profile.status': {
        type: Boolean
    }
});