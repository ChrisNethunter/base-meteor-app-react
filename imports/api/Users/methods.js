import { Meteor } from 'meteor/meteor';
import ValidationsSchemas from '../../startup/server/helpers/ValidationsSchemas';
import { user_schema } from './userSchema';
import { SendEmail } from '../../startup/server/helpers/SendEmail';


Meteor.methods({

  'create.user.account'(user) {
    //FOREVER
    let validate_schema = new ValidationsSchemas(user_schema);
    let test_validate = validate_schema.validate(user);
    if (test_validate.status !== true) {
      throw new Meteor.Error(test_validate.data);
    }
    //FOREVER
    let id_new_user = Accounts.createUser(test_validate.data);
    Roles.addUsersToRoles(id_new_user, user.rol);
    if(user.rol=="client"){
      SendEmail.sendVerification(id_new_user);
    }
    return true
  },

  'get.users.roles'(roles) {
    if (! Meteor.userId()) throw new Meteor.Error("not-authorized");
    
    return Meteor.users.find({ roles : roles }).fetch()
  },
  
  'delete.user'(id) {
    if (! Meteor.userId()) throw new Meteor.Error("not-authorized");
    return Meteor.users.remove({ _id : id });
  },
});