**private folder**

All files inside a top-level directory called `private/` are only accessible from server code and can be loaded via the [`Assets`](http://docs.meteor.com/#/full/assets_getText) API. This can be used for private data files and any files that are in your project directory that you don't want to be accessible from the outside.


## COPIAS BASE DE DATOS HERNANDO

1 - Comando para bajar el archivo .gz, este es el backup cuando se genera 

    ssh root@11.111.111.155 "docker exec mongodb mongodump -d dbname --archive --gzip" > dump-DIA(01)-REEMPLAZARNOMBREMES-2020.gz


2 - Comando para restaurar la copia de la base de datos de manera local, que se bajo con el nombre exacto del archivo

    mongorestore -h 127.0.0.1 --port 3001 -d dbname --gzip --archive=dump-23-enero-2020.gz

3 - Comando para ejecutar mongo en la aplicacion de meteor, debemos estar parados dentro del proyecto para ejecutarlo

    meteor mongo

4 - Con este comando le indicamos que usemos la base de datos local de meteor

    use meteor

5 - Con este comando eliminamos la base de datos meteor, para despues reemplazarla con la base de datos que bajamos de producccion

    db.dropDatabase();

6 - Con este comando le indicamos que la base de datos que se restauro previamente en el paso 2, que se llama prestaly, pase a llamarse meteor

    db.copyDatabase('dbname','meteor');   

Con esto ya tenemos la copia restaurada de manera local

